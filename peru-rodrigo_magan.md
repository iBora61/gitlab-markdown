### First Tutorial
#### Bold and Italic
Writing in Markdown is _not_ that hard  
I **will** complete these lessons!  
"_Of course_," she whispered. Then, she shouted: "All I need is a **little moxie**!  
If you're thinking to yourself, **_This is unbelievable_**, you'd probably be right.  

#### Links
##### Inline links
The Latest News from [the BBC](www.bbc.com/news)

##### Reference links
Do you want to [see something fun][a fun place]?  
Well, do I have [the website for you][another fun place]!  

[a fun place]:www.zombo.com
[another fun place]:www.stumbleupon.com

#### Mermaid
``` mermaid
graph TD;
A-->B;
A-->C;
B-->D;
C-->D;
```
