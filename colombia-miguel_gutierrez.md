Introduction
==========

I am _Miguel Gutierrez_ from **Colombia**. I am a civil engineer with a specialization and experience in the field of: __**structural design.**__


~~~ plantuml

@startmindmap
* Debian
** Ubuntu
*** Linux Mint
*** Kubuntu
*** Lubuntu
*** KDE Neon
** LMDE
** SolydXK
** SteamOS
** Raspbian with a very long name
*** <s>Raspmbc</s> => OSMC
*** <s>Raspyfi</s> => Volumio
@endmindmap

~~~





