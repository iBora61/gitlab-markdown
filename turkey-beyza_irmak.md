# Title

## Subtitle

### Subtitle 

Writing in Markdown is not that hard!

__Writing in Markdown is not that hard!__

_Writing in Markdown is not that hard!_

**Writing in Markdown is not that hard!**

*Writing in Markdown is not that hard!*

**_My name is Beyza Irmak. I studied architecture as a bachelor's degree._**

The name of the school I studied in Istanbul is _Istanbul Technical University._

[Visit GitHub!](www.github.com)

[Google](www.google.com)

[You're **really, really** going to want to see this.](www.dailykitten.com)

#### The Latest News from [the BBC](www.bbc.com/news)

 Here's [a link to something else][another place].
     Here's [yet another link][another-link].
     And now back to [the first link][another place].

     [another place]: www.github.com
     [another-link]: www.google.com



