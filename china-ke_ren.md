# Welcome!

>All the food is delicious!



```plantuml
@startmindmap
* Chinese Food
** staple Food
*** Dumplings
*** Baozi
*** noodles
*** ...

** dishes
*** Guobaorou
*** Laziji
*** ...
@endmindmap

```

* ![Dumplings](https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fzhongces3.sina.com.cn%2Fproduct%2F20211105%2Fbe3fb6c6721f72e990234c0650b7302e.jpeg&refer=http%3A%2F%2Fzhongces3.sina.com.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1652447711&t=56c7aaa5d7c3893581aad04562ff7b8e)

* ![Baozi](https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic167.nipic.com%2Ffile%2F20180604%2F24969966_212603646000_2.jpg&refer=http%3A%2F%2Fpic167.nipic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1652447757&t=f76a0fba6f4379ebf9fe5b4a327add2c)

* ![Guobaorou](https://img2.baidu.com/it/u=1810590982,1612606712&fm=253&fmt=auto&app=138&f=JPEG?w=448&h=320)

* ![Laziji](https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp8.itc.cn%2Fq_70%2Fimages03%2F20201213%2F8baa27971d634f18adcc6a9ec6545fef.jpeg&refer=http%3A%2F%2Fp8.itc.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1652448089&t=18279f222eacadb08135f1c54ea53fa8)


[You can use the Chinese searching engine.](https://www.baidu.com)


```Java
public class StapleFood {
  
  private String name;
  
  private float price;

  public StapleFood(String n, float p) {
    this.name = n;
    this.price = p;
  }

  public static void main(String[] args) {
    StapleFood dumplings = new StapleFood("dumplings", 1.5);
  }

}
```




