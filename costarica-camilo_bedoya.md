Headers on Markdown :monkey:
-----------------------

### Titles on Markdown


Markdown is not that hard!  
Let's try **bold moves**.  
Now some _italics_.  


First Level

>block quotes 
>>embedded block quotes

~~~ python
## Cool Python Code // In Spanish but still ok
def es_primo(number):
    for i in range(2,number - 1):
        if number % i == 0:
            return False
            break
    return True  


def run():
    numero = int(input('Ingrese un numero: '))
    if es_primo(numero) == True:
        print('Es un numero Primo')
    else:
        es_primo(numero) == False
        print('Es un numero no Primo')


if __name__ == '__main__':
    run()

~~~

![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1200px-Python-logo-notext.svg.png)

1. Lists
    1. Embedded lists
2. Lists 

| Header1 | Header2 | Header3 |
|:--------|:-------:|--------:|
| cell1   | cell2   | cell3   |
| cell4   | cell5   | cell6   |


| Table 1 |
|:--------|
| 10.20   |

This is a paragraph
{::comment}
This is a comment which is
completely ignored.
{:/comment}
... paragraph continues here.


{::comment}  
This text is completely ignored by kramdown - a comment in the text.  
{:/comment}  











This is a paragraph  
{::comment}  
This is a comment which is
completely ignored.  
{:/comment}
... paragraph continues here.  

Extensions can also be used
inline {::nomarkdown}**see**{:/}!


<p>This is a paragraph
<!-- 
This is a comment which is
completely ignored.
 -->
… paragraph continues here.</p>

<p>Extensions can also be used
inline **see**!</p>


A [link](https://cr.rwth-aachen.de/) to the CR Master's Website

This is an HTML
example.

*[HTML]: Hyper Text Markup Language


This is *red*{:style="color: red"}.   



