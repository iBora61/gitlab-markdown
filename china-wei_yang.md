# [Online Markdown tutorial](https://www.markdowntutorial.com/) Quick Overview

## _Italics_ and **Bold**

If you're **thinking** to yourself, **_This is unbelievable_**, you'd _probably_ be right.

## Headers
#### Colombian Symbolism in _One Hundred Years of Solitude_

Here's some words about the book _One Hundred Years..._.

## Links and Referrence

[You're **really, really** going to want to see this.](www.dailykitten.com)

Do you want to [see something fun][a fun place]?

Well, do I have [the website for you][another fun place]!

[a fun place]: www.zombo.com
[another fun place]: www.stumbleupon.com

## Images

Tiger with custom width  
<img src="https://upload.wikimedia.org/wikipedia/commons/5/56/Tiger.50.jpg" alt="drawing" width="100"/>

![Black cat][Black]

![Orange cat][Orange]

[Black]: https://upload.wikimedia.org/wikipedia/commons/a/a3/81_INF_DIV_SSI.jpg
[Orange]: http://icons.iconarchive.com/icons/google/noto-emoji-animals-nature/256/22221-cat-icon.png

## Blockquotes
> He left her quickly, fearing that her intimacy might turn to jibing and wishing to be out of the way before she offered her ware to another, a tourist from England or a student of Trinity. Grafton Street, along which he walked, prolonged that moment of discouraged poverty. In the roadway at the head of the street a slab was set to the memory of Wolfe Tone and he remembered having been present with his father at its laying. He remembered with bitterness that scene of tawdry tribute. There were four French delegates in a brake and one, a plump smiling young man, held, wedged on a stick, a card on which were printed the words: _VIVE L'IRLANDE_!

## Lists
* Flour
* Cheese
* Tomatoes

1. Cut the cheese
1. Slice the tomatoes
1. Rub the tomatoes in flour

* Calculus
  * A professor
  * Has no hair
  * Often wears green
* Castafiore
  * An opera singer
  * Has white hair
  * Is possibly mentally unwell

 1. Cut the cheese  
    Make sure that the cheese is cut into little triangles.

 1. Slice the tomatoes  
    Be careful when holding the knife.  
    For more help on tomato slicing, see Thomas Jefferson's seminal essay _Tom Ate Those_.


## Paragraphs
double space at the end of line would make a soft break for a Paragraph

# [kramdown](https://kramdown.gettalong.org/quickref.html)



a small application writen in Rust to say hello.   
~~~rust  

use ferris_says::say; // from the previous step
use std::io::{stdout, BufWriter};

fn main() {
    let stdout = stdout();
    let message = String::from("Hello fellow Rustaceans!");
    let width = message.chars().count();

    let mut writer = BufWriter::new(stdout.lock());
    say(message.as_bytes(), width, &mut writer).unwrap();
}


~~~
which has fallowing ouput
~~~sh
----------------------------
< Hello fellow Rustaceans! >
----------------------------
              \
               \
                 _~^~^~_
             \) /  o o  \ (/
               '_   -   _'
               / '-----' \


~~~



